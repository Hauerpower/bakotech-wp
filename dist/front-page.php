<?php get_header(); ?>

<?php get_template_part('partials/banner'); ?>

<div class="grid-container form-box-bg">

	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/Subtraction1 (2).png" alt="">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/Subtraction1 (2).png" alt="">
	
	<main class="grid-x main-big-box ">

		<div class="large-6 main-box">

			<?php the_field('content2'); ?>
		</div>

		<div class="large-6 form-box">
			<div class="form-box-inside">


				<div class="from-box-inside-index">

					<form action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">


						<input type=hidden name="oid" value="00D20000000oqHA">
						<input type=hidden name="retURL" value="<?php echo get_site_url(); ?>/thank-you">


						<h3><?php the_field('header3'); ?></h3>
						<input type="text" name="first_name" placeholder="Imię" required>
						<hr>
						<input type="text" name="last_name" placeholder="Nazwisko" required>
						<hr>
						<input type="text" name="company" placeholder="Firma" required>
						<hr>
						<!-- było:    <input type="text" name="tel" id="" value="Numer telefonu" onfocus="this.value=''"> -->
						<input type="text" name="phone" id="" placeholder="Numer telefonu" required>
						<hr>
						<input type="text" name="email" id="" placeholder="E-mail" required>
						<hr>
						<input type="text" name="00N20000009P3LN" placeholder="Liczba stanowisk pracy" required>
						<hr>

						<p class="checbox-text"> <input type="checkbox" name="acceptance" id="" required> <?php the_field('acceptance3'); ?> </p>


						<!-- Kod kraju - w tym przypadku na sztywno Polska -->
						<input type="hidden" id="country_code" name="country_code" value="PL" />

						<!-- Opis - -->
						<input type="hidden" id="description" name="description" value="Trial 90 dni" />

						<!-- Zrodlo potencjalnego klienta -->
						<input type="hidden" id="lead_source" name="lead_source" value="Landing Page" />

						<!-- Kampania -->
						<input type="hidden" id="Campaign_ID" name="Campaign_ID" value="7013X0000021HgBQAU" />

						<!-- Newsletter -->
						<input type="hidden" id="00N20000009P3Lc" name="00N20000009P3Lc" value="Yes" />

						<!-- Producent -  -->
						<input type="hidden" id="00N20000009P3M6" name="00N20000009P3M6" value="F-Secure" />

						<!-- Typ Leada -->
						<input type="hidden" id="00N20000009P3MB" name="00N20000009P3MB" value="Customer" />

						<!-- Typ Rekordu -->
						<!-- <input type="hidden"  id="00N0O00000A9cKV" name="00N0O00000A9cKV" value="Lead" /><br> -->


						<div class="button-form-box">
							<button type="submit" name="submit"> WYŚLIJ zgłoszenie</button>
						</div>
					</form>
				</div>
			</div>

		</div>


	</main>

</div>

<?php get_template_part('partials/banner-footer'); ?>

<?php get_footer(); ?>