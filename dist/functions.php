<?php

define('THEME_NS', 'bazowy');


function hp_theme_setup()
{

  load_theme_textdomain(THEME_NS, get_template_directory() . '/languages');

  show_admin_bar(false);

  add_theme_support('post-thumbnails');

  register_nav_menus(array(
    'main-menu' => 'Menu glówne'
  ));
}


add_action('after_setup_theme', 'hp_theme_setup');


require_once 'functions/tgmp.php';

require_once 'functions/scripts.php';

require_once 'functions/translate.php';

require_once 'functions/acf.php';
require_once 'functions/ajax.php';
require_once 'functions/addons.php';
require_once 'functions/cleanup_security.php';
