<header class="header-bg">
    <div class="header-bg-2 large-6">
        <div class="header-bg-1">
            <div class="header-text">
                <h1><?php the_field('header1', 8); ?></h1>
                <h2><?php the_field('header1_2', 8); ?></h2>
                <?php the_field('content1', 8); ?>
            </div>
        </div>
    </div>
</header>