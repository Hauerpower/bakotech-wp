<div class="hide-form ">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Subtraction1 (2).png" alt="">
    <div class="hide-box form-box-size">

        <div class="hide-box-inside">


            <form action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">



                <input type=hidden name="oid" value="00D20000000oqHA">
                <input type=hidden name="retURL" value="<?php echo get_site_url(); ?>/thank-you">

                <input type="checkbox" class="toggler">
                <div class="hamburger">
                    <div></div>
                </div>
                <h3><?php the_field('header4'); ?></h3>
                <input type="text" name="first_name" placeholder="Imię" required>
                <hr>
                <input type="text" name="last_name" placeholder="Nazwisko" required>
                <hr>
                <input type="text" name="company" placeholder="Firma" required>
                <hr>
                <input type="text" name="phone" id="" placeholder="Numer telefonu" required>
                <hr>
                <input type="text" name="email" id="" placeholder="E-mail" required>
                <hr>


                <p class="checbox-text"> <input type="checkbox" name="acceptance" id="" required> <?php the_field('acceptance4'); ?></p>


                <!-- Kod kraju - w tym przypadku na sztywno Polska -->
                <input type="hidden" id="country_code" name="country_code" value="PL" /><br>

                <!-- Opis - -->
                <input type="hidden" id="description" name="description" value="pobranie katalogu Bakotech" /><br>

                <!-- Zrodlo potencjalnego klienta -->
                <input type="hidden" id="lead_source" name="lead_source" value="Landing Page" /><br>

                <!-- Kampania -->
                <input type="hidden" id="Campaign_ID" name="Campaign_ID" value="7013X0000021HgB" /><br>

                <!-- Newsletter -->
                <input type="hidden" id="00N20000009P3Lc" name="00N20000009P3Lc" value="Yes" /><br>

                <!-- Producent -  -->
                <input type="hidden" id="00N20000009P3M6" name="00N20000009P3M6" value="F-Secure" /><br>

                <!-- Typ Leada -->
                <input type="hidden" id="00N20000009P3MB" name="00N20000009P3MB" value="Customer" /><br>

                <!-- Typ Rekordu -->
                <!-- <input type="hidden"  id="00N0O00000A9cKV" name="00N0O00000A9cKV" value="Lead" /><br> -->


                <div class="button-form-box">
                    <button type="submit" name="submit">WYŚLIJ zgłoszenie</button>
                </div>
            </form>

        </div>
    </div>
</div>