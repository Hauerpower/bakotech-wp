<?php $fb = get_translated_option('facebook');
$li = get_translated_option('linkedin');
$tw = get_translated_option('twitter');
$yt = get_translated_option('youtube');
$image = get_translated_option('logo'); ?>
<nav>

	<div class="grid-x header-box">
		<div class="large-6 logo">
			<a href="<?php echo get_site_url(); ?>">
				<?php
				if (!empty($image)) : ?>
					<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
				<?php endif; ?>

			</a>
		</div>
		<div class="large-6 social-media">
			<a href="<?php echo $fb; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb.svg" alt=""></a>
			<a href="<?php echo $li; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/linkedin (1).svg" alt=""></a>
			<a href="<?php echo $tw; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter (1).svg" alt=""></a>
			<a href="<?php echo $yt; ?>" target="_blank" class="last-img"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/youtube (2).svg" alt=""></a>
		</div>
	</div>

</nav>