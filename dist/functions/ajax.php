<?php

function atom_loadmore_ajax_handler(){

	// prepare our arguments for the query
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
	$args['post_status'] = 'publish';
	
	$template = $_POST['template'];
	
	query_posts( $args );
 
	if( have_posts() ) :
		// run the loop
		while( have_posts() ): the_post();
		
			$ajax_load = true;
			include( locate_template($template) ); 
 
		endwhile;
 
	endif;
		
	die; // here we exit the script and even no wp_reset_query() required!
}
  
add_action('wp_ajax_loadmore', 'atom_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'atom_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}
