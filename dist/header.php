<!doctype HTML>
<html class="no-js" <?php language_attributes() ?>>

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
  <title><?php wp_title() ?></title>

  <!-- Global site tag (gtag.js) - Google Ads: 936871077 -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=AW-936871077"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'AW-936871077');
  </script>

  <?php if (is_page(54)) : ?>
    <!-- Event snippet for Website lead conversion page -->
    <script>
      gtag('event', 'conversion', {
        'send_to': 'AW-936871077/rhkCCJC7lc4BEKWJ3r4D'
      });
    </script>
  <?php endif; ?>
  
  <?php wp_head() ?>
</head>

<?php $has_fixed_nav = true; ?>


<?php $fixed_nav_class = $has_fixed_nav ? 'fixed-nav' : ''; ?>

<body <?php body_class($fixed_nav_class) ?>>

  <?php get_template_part('partials/general/site-header'); ?>

  <main id="site-wrap">