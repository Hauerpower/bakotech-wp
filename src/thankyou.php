<?php
/*Template name: Thank you*/
get_header(); ?>

<div class="form-box-bg thankyou-wrap">

    <div class="grid-x" style="width:100%; text-align: center;">

        <div class="small-12">

            <h2><?php the_field('header');?></h2>
            <?php the_field('content');?>
            
        </div>

    </div>

</div>

<?php get_template_part('partials/banner'); ?>

<?php get_footer() ?>