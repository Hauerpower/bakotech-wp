<?php
$image1 = get_translated_option('logo2');
$image2 = get_translated_option('logo_2');
$image3 = get_translated_option('logo_3');
$address = get_translated_option('address');
$contact = get_translated_option('contact'); ?>
<div class="grid-container ">
	<div class="grid-x footer-main">
		<div class="footer-box footer-box-1">
			<div>
				<?php
				if (!empty($image1)) : ?>
					<img src="<?php echo esc_url($image1['url']); ?>" alt="<?php echo esc_attr($image1['alt']); ?>" />
				<?php endif; ?>

			</div>
			<div>
				<?php echo $address; ?>
			</div>

			<div>
				<?php echo $contact; ?>
			</div>
		</div>
		<div class="footer-box footer-box-2">
			<div>
				<?php
				if (!empty($image2)) : ?>
					<img src="<?php echo esc_url($image2['url']); ?>" alt="<?php echo esc_attr($image2['alt']); ?>" />
				<?php endif; ?>
			</div>
			<div>
				<?php
				if (!empty($image3)) : ?>
					<img src="<?php echo esc_url($image3['url']); ?>" alt="<?php echo esc_attr($image3['alt']); ?>" />
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>