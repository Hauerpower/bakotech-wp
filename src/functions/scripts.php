<?php

function atom_scripts(){
	
	global $wp_query;

	wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css?family=Noto+Sans:400,700&display=swap', array(), '', 'all' );
	wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/app.css', array(), filemtime( get_template_directory() . '/assets/css/app.css' ), 'all');

	// wp_enqueue_script('libs', get_template_directory_uri() . '/assets/js/libs.js', array(), filemtime( get_template_directory() . '/assets/js/libs.js' ), true);
	
	wp_register_script('main', get_template_directory_uri() . '/assets/js/app.js', array('jquery'), filemtime( get_template_directory() . '/assets/js/app.js' ), true);
	wp_localize_script('main', 'wp_params', array(
	    'ajax_url' => admin_url( 'admin-ajax.php' ),
	    'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
	    'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
	    'max_page' => $wp_query->max_num_pages,
	));
	wp_enqueue_script('main');
	
}
add_action('wp_enqueue_scripts', 'atom_scripts');


