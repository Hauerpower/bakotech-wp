<?php
	
function my_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyCIp2clkIsq6bcRX7o49liGEulddjM35jI';
	return $api;	
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

	
if( function_exists('acf_add_options_page') ){
	
	acf_add_options_page(array(
		'page_title' => a__('Opcje strony'),
		'menu_slug' => 'acf-options'
	));
		
	if( function_exists('pll_languages_list') ){
		foreach (pll_languages_list() as $lang) {
		
		  acf_add_options_sub_page([
		    'page_title' => a__("Opcje") . " - " . $lang,
		    'menu_title' => a__("Opcje") . " - " . $lang,
		    'menu_slug' => "acf-options-". $lang,
		    'post_id' => $lang,
		    'parent' => 'acf-options'
		  ]);
		
		}
	}

}

function add_acf_submenu_options($slug, $title, $parent){

	if( function_exists('pll_languages_list') ){
		foreach (pll_languages_list() as $lang) {
		
		  acf_add_options_sub_page([
		    'page_title' => a__($title) . " - " . $lang,
		    'menu_title' => a__("Opcje") . " - " . $lang,
		    'menu_slug' => $slug . '-'. $lang,
		    'post_id' => $slug . '-' . $lang,
		    'parent' => $parent
		  ]);
		
		}
	}
	else {
		
	  acf_add_options_sub_page([
	    'page_title' => a__($title),
	    'menu_title' => a__("Opcje"),
	    'menu_slug' => $slug,
	    'post_id' => $slug,
	    'parent' => $parent
	  ]);
		
	}

}

function get_translated_option($option, $options_id = false){
	if($options_id){
		if( function_exists('pll_current_language') ) return get_field($option, $options_id . '-' . pll_current_language());
		return get_field($option, $options_id); 
		
	}
	else {
		if( function_exists('pll_current_language') ) return get_field($option, pll_current_language());
		return get_field($option, 'option'); 
	}
}

function get_translated_options_id($options_id = false){
	if($options_id){
		if( function_exists('pll_current_language') ) return $options_id . '-' . pll_current_language();
		return $options_id;
		
	}
	else {
		if( function_exists('pll_current_language') ) return pll_current_language();
		return 'option';
	}
}

function custom_acf_location_name($values, $rule){
	
	$option_pages = acf_get_options_pages();
	
	foreach($values as $slug => $value){
		if( isset($option_pages[$slug]) ){
			$values[$slug] = $option_pages[$slug]['page_title'];
		}
	}
	
	return $values;
	
}
add_filter("acf/location/rule_values/options_page", 'custom_acf_location_name', 10, 2);


function acf_orphans($value, $post_id, $field) {
  if ( class_exists( 'iworks_orphan' ) ) {
    $orphan = new \iworks_orphan();
    $value = $orphan->replace( $value );
  }
  return $value;
}
add_filter('acf/format_value/type=textarea', 'acf_orphans', 10, 3);
add_filter('acf/format_value/type=wysiwyg', 'acf_orphans', 10, 3);
